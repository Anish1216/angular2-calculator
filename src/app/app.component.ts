import { Component } from '@angular/core';
import { CalculatorComponent } from './calc.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent { // Main class w/ decorator
  names: string[];
  calcHeader: string;

  constructor() {
    this.names = ['Andrew', 'Anish', 'Jeff', 'Nolan', 'Ryan'];
    this.calcHeader = 'Anish Krishnan\'s Angular 2 Calculator';
  }
}
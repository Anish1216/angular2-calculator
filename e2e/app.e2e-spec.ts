import { CalculatorProjectPage } from './app.po';

describe('calculator-project App', function() {
  let page: CalculatorProjectPage;

  beforeEach(() => {
    page = new CalculatorProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
